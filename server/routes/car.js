const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

router.post('/add', (request, response) => {
    const { id, name, model, price, carColor } = request.body
  
    const statement = `
      insert into cartb
        (id,name,model,price,carColor)
      values 
        ('${id}', '${name}', '${model}', '${price}','${carColor}')
    `
  
    const connection = db.openConnection()
    connection.query(statement, (error, result) => {
      connection.end()
      response.send(utils.createResult(error, result))
    })
  })


  router.delete('/delete:id', (request, response) => {
    const { id } = request.params
  
    const statement = `
      delete from cartb
      where
        id = ${id}
    `
  
    const connection = db.openConnection()
    connection.query(statement, (error, result) => {
      connection.end()
      response.send(utils.createResult(error, result))
    })
  })
  router.get('/search', (request, response) => {
   
    //id | name   | model | price | carColor
    const statement = `
      select
        id,
        name,
        model,price,carColor
       
      from cartb
     
    `
    const connection = db.openConnection()
    connection.query(statement, (error, records) => {
      connection.end()
      if (records.length > 0) {
        response.send(utils.createResult(error, records[0]))
      } else {
        response.send(utils.createResult(' does not exist'))
      }
    })
  })
  

 

  
  module.exports = router