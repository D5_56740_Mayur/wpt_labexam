import Signin from './pages/Signin'
// import AddCar from './pages/AddCar'
// import AddCar from './pages/DeleteCar'
// import AddCar from './pages/ShowAll'

import { BrowserRouter, Route, Routes, Link } from 'react-router-dom'
import { ToastContainer } from 'react-toastify'




function App() {
  return (
    <div className="container">
     
      <BrowserRouter>
        <Routes>
            
          <Route path="/signin" element={<Signin />} />
          
         
        </Routes>
      </BrowserRouter>
      <ToastContainer theme="colored" />
    </div>
  );
}

export default App;
