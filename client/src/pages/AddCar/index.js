import axios from 'axios'
import { useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import { URL } from '../../config'
//id | name   | model | price | carColor
const AddCar = () => {
    const [id, setId] = useState('')
    const [name, setName] = useState('')
    const [model, setModel] = useState('')
    const [price, setPrice] = useState('')
    const [carColor, setCarColor] = useState('')
  
    const navigate = useNavigate()

    const save = () => {
        if (name.length == 0) {
          toast.warning('please enter name')
        } else if (model.length == 0) {
          toast.warning('please enter model')
        } else if (price.length == 0) {
          toast.warning('please enter price')
        } else {
          const body = {
            id,
            name,
           model,
           price,
           carColor
          }
    
          const url = `${URL}/car/`
          axios.post(url, body).then((response) => {
            const result = response.data
            if (result['status'] == 'success') {
              toast.success('added new car..')
              navigate('/showall')
            } else {
              toast.error(result['error'])
            }
          })
        }
}
return (
    <div>
      <h1 className="title">Add Car</h1>

      <div className="form">
        <div className="mb-3">
          <label htmlFor="" className="label-control">
            Id
          </label>
          <input
            onChange={(e) => {
              setId(e.target.value)
            }}
            type="text"
            className="form-control"
          />
        </div>

        <div className="mb-3">
          <label htmlFor="" className="label-control">
            Name
          </label>
          <input
            onChange={(e) => {
              setName(e.target.value)
            }}
            type="text"
            className="form-control"
          />
        </div>

        <div className="mb-3">
          <label htmlFor="" className="label-control">
            Model
          </label>
          <textarea
            onChange={(e) => {
              setModel(e.target.value)
            }}
            rows="10"
            className="form-control"
          ></textarea>
        </div>

        <div className="mb-3">
          <label htmlFor="" className="label-control">
            Price
          </label>
          <textarea
            onChange={(e) => {
              setPrice(e.target.value)
            }}
            rows="10"
            className="form-control"
          ></textarea>
        </div>

        <div className="mb-3">
          <label htmlFor="" className="label-control">
            CarColor
          </label>
          <textarea
            onChange={(e) => {
              setCarColor(e.target.value)
            }}
            rows="10"
            className="form-control"
          ></textarea>
        </div>

        <div className="mb-3">
          <button onClick={save} className="btn btn-success">
            Save
          </button>
          <Link to="/home" className="btn btn-danger float-end">
            Cancel
          </Link>
        </div>
      </div>
    </div>
  )
}

export default AddCar